use std::collections::HashMap;
use std::collections::HashSet;
use std::env;

fn main() {
    let input = std::fs::read_to_string(&env::args().nth(1).unwrap()).expect("Could not read file");

    // part 1
    let part_1 = |group: &str| {
        group
            .chars()
            .filter(|c| c.is_ascii_alphabetic())
            .collect::<HashSet<char>>()
            .len()
    };

    print_sum(&input, part_1);

    // part 2
    let part_2 = |group: &str| {
        let mut answers_with_counts = HashMap::new();

        group.split('\n').for_each(|person| {
            person
                .chars()
                .filter(|c| c.is_ascii_alphabetic())
                .for_each(|a| {
                    let count = answers_with_counts.entry(a).or_insert(0);
                    *count += 1;
                })
        });

        answers_with_counts
            .iter()
            .filter(|(_, count)| *count == &group.split('\n').count())
            .count()
    };

    print_sum(&input, part_2);
}

fn print_sum<F>(input: &str, f: F) where F: Fn(&str) -> usize {
    println!(
        "In total [{}] people answered with 'yes'",
        input.split("\n\n").map(f).sum::<usize>()
    );
}
