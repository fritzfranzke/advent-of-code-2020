use std::env;
use std::collections::BTreeMap;

fn main() {
    let mut adapters: Vec<usize> = std::fs::read_to_string(&env::args().nth(1).unwrap())
        .expect("Could not read file")
        .lines()
        .map(|line| line.parse::<usize>().unwrap())
        .collect();

    adapters.sort_unstable();
    adapters.insert(0, 0); // add joltage of outlet
    adapters.push(adapters.last().unwrap() + 3);

    // part 1
    let joltages = count_jolt_differences(&adapters);
    println!("There are [{}] differences of 1 jolt and [{}] differences of three jolts, multiplied [{}].", joltages.0, joltages.1, joltages.0 * joltages.1);

    // part 2
    println!(
        "Number of arrangements: {}",
        count_with_cache(&adapters, 0, &mut BTreeMap::new())
    )
}

fn count_jolt_differences(joltages: &[usize]) -> (usize, usize) {
    let mut one_jolt_differences = 0;
    let mut three_jolt_differences = 0;

    joltages.windows(2).for_each(|window| {
        if window[1] - window[0] == 1 {
            one_jolt_differences += 1
        } else {
            three_jolt_differences += 1
        }
    });

    (one_jolt_differences, three_jolt_differences)
}

// solution taken from @nicolashahn
// https://github.com/nicolashahn/advent-of-code/blob/master/2020/d10/src/main.rs
fn count_with_cache(adapters: &[usize], i: usize, cache: &mut BTreeMap<usize, usize>) -> usize {
    if cache.contains_key(&i) {
        return *cache.get(&i).unwrap();
    }

    if i == adapters.len() - 1 {
        return 1;
    }

    let mut result = 0;
    for j in (i + 1)..=(i + 3) {
        if j < adapters.len() && adapters[j] - adapters[i] <= 3 {
            result += count_with_cache(&adapters, j, cache);
        }
    }

    cache.insert(i, result);

    result
}


// recursive functions w/o optimization

fn count_valid_arrangements_top_down (
    current_joltage: usize,
    remaining_adapters: &[usize],
) -> usize {
    if !remaining_adapters.is_empty() {
        remaining_adapters
            .iter()
            .rev()
            .enumerate()
            .filter(|(_, value)| current_joltage as isize - **value as isize <= 3) // isize to prevent overflow when subtracting
            .map(|(index, value)| {
                count_valid_arrangements_top_down(*value, &remaining_adapters[..remaining_adapters.len() - index - 1])
            })
            .sum()
    } else if current_joltage as isize - 3 <= 0 {
        1
    } else {
        0
    }
}

fn count_valid_arrangements_bottom_up(
    current_joltage: usize,
    max_joltage: usize,
    remaining_adapters: &[usize],
) -> usize {
    if !remaining_adapters.is_empty() {
        remaining_adapters
            .iter()
            .enumerate()
            .filter(|(_, value)| **value as isize - current_joltage as isize <= 3) // isize to prevent overflow when subtracting
            .map(|(index, value)| {
                count_valid_arrangements_bottom_up(*value, max_joltage, &remaining_adapters[index + 1..])
            })
            .sum()
    } else if max_joltage - current_joltage <= 3 {
        1
    } else {
        0
    }
}
