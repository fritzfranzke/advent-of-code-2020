use std::env;

const ROWS: std::ops::RangeInclusive<u16> = 0..=127;
const COLUMNS: std::ops::RangeInclusive<u16> = 0..=7;

fn main() {
    // part 1
    let max_seat_id = std::fs::read_to_string(&env::args().nth(1).unwrap())
        .expect("Could not read file")
        .lines()
        .map(|line| {
            binary_space_partition(&line[..7], ROWS, 'F', 'B') * 8
                + binary_space_partition(&line[7..], COLUMNS, 'L', 'R')
        })
        .max()
        .unwrap();

    println!("Max seat ID is [{}]", max_seat_id);

    // part 2
    let mut seats = Vec::new();

    for row in ROWS {
        for column in COLUMNS {
            seats.push(row * 8 + column)
        }
    }

    seats.sort_unstable();

    std::fs::read_to_string(&env::args().nth(1).unwrap())
        .expect("Could not read file")
        .lines()
        .map(|line| {
            binary_space_partition(&line[..7], ROWS, 'F', 'B') * 8
                + binary_space_partition(&line[7..], COLUMNS, 'L', 'R')
        })
        .for_each(|id| seats.retain(|seat| *seat != id));

    println!(
        "Empty seat is {:?}",
        seats
            .iter()
            .filter(|&&x| x > COLUMNS.max().unwrap() && x <= max_seat_id)
            .collect::<Vec<&u16>>()
    );
}

fn binary_space_partition(
    seat_position: &str,
    partition: std::ops::RangeInclusive<u16>,
    lower_key: char,
    upper_key: char,
) -> u16 {
    let min = partition.clone().min().unwrap();
    let max = partition.max().unwrap();

    match (seat_position.chars().next().unwrap(), max - min) {
        (l, 1) if l == lower_key => min,
        (u, 1) if u == upper_key => max,
        (l, _) if l == lower_key => binary_space_partition(
            &seat_position[1..],
            min..=(min + ((max - min) / 2)),
            lower_key,
            upper_key,
        ),
        (u, _) if u == upper_key => binary_space_partition(
            &seat_position[1..],
            (min + ((max - min) / 2) + 1)..=max,
            lower_key,
            upper_key,
        ),
        _ => panic!("Invalid partition"),
    }
}
