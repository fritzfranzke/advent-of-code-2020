use std::env;

fn main() {
    let mut seat_layout: Vec<Vec<char>> = Vec::new();

    std::fs::read_to_string(&env::args().nth(1).unwrap())
        .expect("Could not read file")
        .lines()
        .for_each(|line| seat_layout.push(line.chars().into_iter().collect()));

    // part 1
    println!(
        "Part 1: Counted [{}] occupied seats",
        get_final_layout(&seat_layout, &count_occupied_adjacent_seats(), 4)
            .iter()
            .map(|row| row.iter().filter(|seat| **seat == '#').count())
            .sum::<usize>()
    );

    // part 1
    println!(
        "Part 2: Counted [{}] occupied seats",
        get_final_layout(&seat_layout, &count_occupied_visible_seats(), 5)
            .iter()
            .map(|row| row.iter().filter(|seat| **seat == '#').count())
            .sum::<usize>()
    )
}

fn get_final_layout<F>(layout: &[Vec<char>], f: &F, threshold_occupied_seats: usize) -> Vec<Vec<char>>
where
    F: Fn(&[Vec<char>], (isize, isize)) -> usize,
{
    let mut result = layout.to_vec();

    loop {
        let new_layout = transform(&result, f, threshold_occupied_seats);

        if new_layout == result {
            // no more change in state
            result = new_layout;
            break;
        }

        result = new_layout;
    }

    result
}

fn transform<F>(layout: &[Vec<char>], f: &F, threshold_occupied_seats: usize) -> Vec<Vec<char>>
where
    F: Fn(&[Vec<char>], (isize, isize)) -> usize,
{
    let mut result = Vec::new();

    layout.iter().enumerate().for_each(|(row_number, seats)| {
        let mut new_row = Vec::new();

        seats.iter().enumerate().for_each(|(seat_number, seat)| {
            let occupied_seats = f(layout, (row_number as isize, seat_number as isize));

            new_row.push(if *seat == 'L' && occupied_seats == 0 {
                '#'
            } else if *seat == '#' && occupied_seats >= threshold_occupied_seats {
                'L'
            } else {
                *seat
            });
        });

        result.push(new_row);
    });

    result
}

// part 1
fn count_occupied_adjacent_seats() -> impl Fn(&[Vec<char>], (isize, isize)) -> usize {
    |layout: &[Vec<char>], seat: (isize, isize)| -> usize {
        let adjacent_seats: [(isize, isize); 8] = [
            (seat.0 - 1, seat.1 - 1),
            (seat.0 - 1, seat.1),
            (seat.0 - 1, seat.1 + 1),
            (seat.0, seat.1 - 1),
            (seat.0, seat.1 + 1),
            (seat.0 + 1, seat.1 - 1),
            (seat.0 + 1, seat.1),
            (seat.0 + 1, seat.1 + 1),
        ];

        adjacent_seats
            .iter()
            .filter(|seat| seat.0 >= 0 && seat.1 >= 0)
            .filter_map(|seat| layout.get(seat.0 as usize)?.get(seat.1 as usize))
            .filter(|seat| **seat == '#')
            .count()
    }
}

// part 2
fn count_occupied_visible_seats() -> impl Fn(&[Vec<char>], (isize, isize)) -> usize {
    |layout: &[Vec<char>], seat: (isize, isize)| -> usize {
        let directions: [(isize, isize); 8] = [
            (-1, - 1),
            (-1, 0),
            (-1, 1),
            (0, -1),
            (0, 1),
            (1, -1),
            (1, 0),
            (1, 1),
        ];

        directions
            .iter()
            .filter(|direction| is_direction_blocked(&layout, seat, **direction))
            .count()
    }
}

fn is_direction_blocked(
    layout: &[Vec<char>],
    current_position: (isize, isize),
    modifier: (isize, isize),
) -> bool {
    let mut origin = current_position;

    while let Some(next_position) = get_next_position(&layout, origin, modifier) {
        if next_position == &'#' {
            return true;
        } else if next_position == &'L' {
            return false;
        }

        origin = (origin.0 + modifier.0, origin.1 + modifier.1);
    }

    false
}

fn get_next_position(
    layout: &[Vec<char>],
    current_position: (isize, isize),
    modifier: (isize, isize),
) -> Option<&char> {
    layout.get((current_position.0 + modifier.0) as usize)?.get((current_position.1 + modifier.1) as usize)
}
