use regex::Regex;
use std::env;

fn main() {
    let regex = Regex::new(r"(\d+)-(\d+) (.): ([a-z]+)").unwrap();

    let args: Vec<String> = env::args().collect();

    let content = std::fs::read_to_string(&args[1]).expect("Could not read file");

    let valid_passwords = content
        .lines()
        .filter(|line| {
            let constraint = regex.captures(line).unwrap();
            let number_1 = constraint
                .get(1)
                .map_or("0", |min| min.as_str())
                .parse::<usize>()
                .expect("Invalid min number encountered as constraint");
            let number_2 = constraint
                .get(2)
                .map_or("0", |max| max.as_str())
                .parse::<usize>()
                .expect("Invalid max number encountered as constraint");

            let letter = constraint
                .get(3)
                .map_or("", |l| l.as_str())
                .chars()
                .next()
                .unwrap();

            let password: Vec<char> = constraint
                .get(4)
                .map_or("", |pw| pw.as_str())
                .chars()
                .collect();

            match (password.get(number_1 - 1), password.get(number_2 - 1)) {
                (Some(char_1), Some(char_2)) => {
                    (char_1 == &letter || char_2 == &letter)
                        && !(char_1 == &letter && char_2 == &letter)
                }
                _ => false,
            }

            /* Solution for part 1
            (number_1..number_2 + 1).contains(&constraint
                .get(4)
                .map_or("", |pw| pw.as_str())
                .matches(letter)
                .count())
             */
        })
        .count();

    println!(
        "There are [{}] valid passwords in the database.",
        valid_passwords
    );
}
