# Advent of Code 2020

Contains solutions for the 2020 Advent of Code, in Rust.

## How to run
Enter directory of choice and run `cargo run -- input.txt`.
