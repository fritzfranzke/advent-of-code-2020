use std::collections::HashSet;
use std::env;
use std::str::FromStr;

#[derive(Debug, Clone, PartialEq)]
enum Operation {
    Nop,
    Acc,
    Jmp,
}

impl FromStr for Operation {
    type Err = ();

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "nop" => Ok(Operation::Nop),
            "acc" => Ok(Operation::Acc),
            "jmp" => Ok(Operation::Jmp),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Clone)]
struct Instruction {
    operation: Operation,
    argument: isize,
}

fn main() {
    let instructions: Vec<Instruction> = std::fs::read_to_string(&env::args().nth(1).unwrap())
        .expect("Could not read file")
        .lines()
        .map(|line| {
            let mut instruction = line.split(' ');
            let operation =
                Operation::from_str(&instruction.next().unwrap().to_lowercase()).unwrap();

            Instruction {
                operation,
                argument: instruction.next().unwrap().parse::<isize>().unwrap()
            }
        })
        .collect();

    // part 1
    count_accumulator(&instructions, false);

    // part 2
    get_permutations(&instructions)
        .iter()
        .for_each(|permutation| count_accumulator(&permutation, true))
}

fn count_accumulator(instructions: &[Instruction], suppress_infinite: bool) {

    let mut current_instruction: isize = 0;
    let mut completed_instructions = HashSet::new();
    let mut accumulator: isize = 0;

    loop {
        if !completed_instructions.insert(current_instruction) {
            if !suppress_infinite {
                println!("Infinite loop! Accumulator is [{}]", accumulator);
            }
            break;
        } else if let Some(instruction) = instructions.get(current_instruction as usize) {
            match instruction.operation {
                Operation::Nop => {
                    current_instruction += 1;
                }
                Operation::Acc => {
                    current_instruction += 1;
                    accumulator += instruction.argument;
                }
                Operation::Jmp => {
                    current_instruction += instruction.argument;
                }
            }
        } else {
            println!("Terminated! Accumulator is [{}]", accumulator);
            break;
        }
    }
}

// compute all possible changes from nop to jmp and vice versa
fn get_permutations(input: &[Instruction]) -> Vec<Vec<Instruction>> {
    let mut result = Vec::new(); // a set would require manual implementation of hash for custom structs

    for (index, instruction) in input.iter().enumerate() {
        if instruction.operation == Operation::Nop || instruction.operation == Operation::Jmp {
            let mut permutation = input.to_vec();

            permutation.get_mut(index).unwrap().operation =
                if permutation.get(index).unwrap().operation == Operation::Nop {
                    Operation::Jmp
                } else {
                    Operation::Nop
                };

            result.push(permutation);
        }
    }

    result
}
