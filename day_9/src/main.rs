use std::env;

const PREAMBLE_SIZE: usize = 25;

fn main() {
    let numbers: Vec<usize> = std::fs::read_to_string(&env::args().nth(1).unwrap())
        .expect("Could not read file")
        .lines()
        .map(|line| line.parse::<usize>().unwrap())
        .collect();

    // part 1
    if let Some(outlier) = find_outlier(&numbers) {
        println!("Outlier: {:?}", outlier);

        // part 2
        if let Some(contiguous_set) = find_contiguous_set(&numbers, outlier) {
            let mut sorted = contiguous_set.to_vec();
            sorted.sort_unstable();

            println!(
                "Encryption weakness: {:?}",
                sorted.first().unwrap() + sorted.last().unwrap()
            )
        } else {
            println!("No contiguous set found!")
        }
    } else {
        println!("No outlier found!")
    }
}

fn find_contiguous_set(numbers: &[usize], outlier: usize) -> Option<&[usize]> {
    for i in 0..numbers.len() {
        let mut offset: usize = 0;
        let mut sum = *numbers.get(i).unwrap();

        while sum < outlier {
            offset += 1;
            sum += numbers.get(i + offset).unwrap();
        }

        if sum == outlier {
            return Some(&numbers[i..=i + offset]);
        }
    }

    None
}

fn find_outlier(numbers: &[usize]) -> Option<usize> {
    numbers
        .windows(PREAMBLE_SIZE + 1)
        .find(|window| {
            !contains_valid_pair(
                &window[..window.len() - 1].to_vec(),
                window[window.len() - 1],
            )
        })
        .map(|window| window[window.len() - 1])
}

fn contains_valid_pair(preamble: &[usize], sum: usize) -> bool {
    let mut contains_valid_pair = false;

    for i in preamble.iter() {
        for j in preamble.iter() {
            if i != j && i + j == sum {
                contains_valid_pair = true
            }
        }
    }

    contains_valid_pair
}
