use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();

    let content = std::fs::read_to_string(&args[1]).expect("Could not read file");

    'outer: for line_1 in content.lines() {
        for line_2 in content.lines() {
            for line_3 in content.lines() {
                let number_1 = line_1.parse::<u32>().expect("Not a number");
                let number_2 = line_2.parse::<u32>().expect("Not a number");
                let number_3 = line_3.parse::<u32>().expect("Not a number");

                if number_1 + number_2 + number_3 == 2020 {
                    println!(
                        "Product of 2020 numbers [{}, {}, {}] is [{}]",
                        number_1,
                        number_2,
                        number_3,
                        number_1 * number_2 * number_3
                    );
                    break 'outer;
                }
            }
        }
    }
}
