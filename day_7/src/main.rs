use regex::Regex;
use std::collections::HashMap;
use std::env;

fn main() {
    let extract_container_bag: regex::Regex = Regex::new(r"([a-z]+ [a-z]+)").unwrap();
    let extract_contained_bags: regex::Regex = Regex::new(r"(\d+) ([a-z]+ [a-z]+)").unwrap();

    let bag_color = "shiny gold";

    let mut rules: HashMap<String, HashMap<String, usize>> = HashMap::new();

    std::fs::read_to_string(&env::args().nth(1).unwrap())
        .expect("Could not read file")
        .lines()
        .for_each(|rule| {
            let parent = extract_container_bag
                .captures(rule)
                .unwrap()
                .iter()
                .next()
                .unwrap()
                .unwrap()
                .as_str()
                .to_string();

            let mut containable = HashMap::new();

            extract_contained_bags.captures_iter(rule).for_each(|bag| {
                containable.insert(bag[2].to_owned(), bag[1].parse::<usize>().unwrap());
            });

            rules.insert(parent, containable);
        });


    // part 1

    let number_of_bags = rules
        .keys()
        .filter(|rule| can_be_contained(&rules, &rule, bag_color))
        .count();

    println!(
        "The number of bag colors that can eventually contain at least one [{}] bag is [{}]",
        bag_color, number_of_bags
    );


    // part 2

    let bag_count: usize = count_bags(&rules, bag_color);

    println!(
        "A single [{}] bag must contain [{}] other bags",
        bag_color, bag_count
    );
}

fn can_be_contained(
    rules: &HashMap<String, HashMap<String, usize>>,
    parent: &str,
    bag_color: &str,
) -> bool {
    rules.contains_key(parent)
        && rules
            .get(parent)
            .unwrap()
            .keys()
            .fold(false, |previous, key| {
                previous || key == bag_color || can_be_contained(rules, key, bag_color)
            })
}

fn count_bags(rules: &HashMap<String, HashMap<String, usize>>, colour: &str) -> usize {
    rules
        .get(colour)
        .unwrap()
        .iter()
        .map(|(bag, count)| count + count * count_bags(rules, bag))
        .sum()
}
