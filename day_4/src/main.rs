use regex::Regex;
use std::env;

fn main() {
    let pid = Regex::new(r"\bpid:(\d{9})\b").unwrap();
    let byr = Regex::new(r"\bbyr:(\d{4})\b").unwrap();
    let iyr = Regex::new(r"\biyr:(\d{4})\b").unwrap();
    let eyr = Regex::new(r"\beyr:(\d{4})\b").unwrap();
    let hgt = Regex::new(r"\bhgt:(\d{2}in|\d{3}cm)\b").unwrap();
    let hcl = Regex::new(r"\bhcl:#([[:xdigit:]]{6})\b").unwrap();
    let ecl = Regex::new(r"\becl:(amb|blu|brn|gry|grn|hzl|oth)\b").unwrap();

    let passports = std::fs::read_to_string(&env::args().nth(1).unwrap())
        .expect("Could not read file")
        .split("\n\n")
        .filter(|passport| {
            pid.is_match(passport)
                && if let Some(birth_year) = byr.captures(passport) {
                    (1920..=2002).contains(
                        &birth_year
                            .get(1)
                            .map_or("0", |input| input.as_str())
                            .parse()
                            .unwrap(),
                    )
                } else {
                    false
                }
                && if let Some(issue_year) = iyr.captures(passport) {
                    (2010..=2020).contains(
                        &issue_year
                            .get(1)
                            .map_or("0", |input| input.as_str())
                            .parse()
                            .unwrap(),
                    )
                } else {
                    false
                }
                && if let Some(expiration_year) = eyr.captures(passport) {
                    (2020..=2030).contains(
                        &expiration_year
                            .get(1)
                            .map_or("0", |input| input.as_str())
                            .parse()
                            .unwrap(),
                    )
                } else {
                    false
                }
                && if let Some(h) = hgt.captures(passport) {
                    let height = h.get(1).map_or("0cm", |input| input.as_str());

                    if height.contains("cm") {
                        (150..=193).contains(&(&height[..height.len() - 2]).parse::<u16>().unwrap())
                    } else {
                        (59..=76).contains(&(&height[..height.len() - 2]).parse::<u16>().unwrap())
                    }
                } else {
                    false
                }
                && hcl.is_match(passport)
                && ecl.is_match(passport)
        })
        .count();

    println!("There are [{}] valid passports", passports)
}
